const express = require('express')
const usersRoutes = require('./routes/login')
const app = express()

const port = process.env.PORT || 3000
const host = '0.0.0.0' 

app.listen(port, host, () => {
    console.log( 
        `App is run at http://localshost:${port}`
    )
})
usersRoutes(app)
module.exports = {
    app
}
